create database DBClinicaJS
go
use DBClinicaJS

create table Cliente(
IdCliente int primary key identity not null,
NombreClient varchar(60) not null,
ApellidoPat varchar(50) not null,
ApellidoMat varchar(50) not null,
FechaNacimient datetime not null,
Direccion varchar(100) not null,
Ci int not null,
Telefono int null,
Email varchar(50) not null,
);

insert into Cliente values('Jairo','Barja','Siles','19-03-2019','Km14 Barrio Simon Bolivar',8082095,75598069,'jairobarja@gmail.com')

select * from Cliente